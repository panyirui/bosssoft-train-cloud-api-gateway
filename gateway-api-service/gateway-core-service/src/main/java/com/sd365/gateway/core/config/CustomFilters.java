package com.sd365.gateway.core.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@RefreshScope
@Data
@Configuration
@ConfigurationProperties("custom")
public class CustomFilters {
    private List<String> filters;
}
