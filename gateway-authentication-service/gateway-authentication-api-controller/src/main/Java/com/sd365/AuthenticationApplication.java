package com.sd365;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
/**
 * @class AuthenticationApplication
 * @classdesc 负责网关的认证服务，登录的请求将转发到该服务进行处理
 * <p>维护历史：
 *   2021-11-17 优化pom文件 去除冗余 引入spring-cloud-stater-alibaba-naocs-config<br>
 *   2021-11-17 增加 bootstrap.yml 将 application-xx 转化为nacos配置中文文件
 * </p>
 * @author Administrator
 * @date 2021-11-17  17:56
 * @version 1.0.0
 * @see
 * @since
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableSwagger2
@RefreshScope
public class AuthenticationApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthenticationApplication.class, args);
    }
}
