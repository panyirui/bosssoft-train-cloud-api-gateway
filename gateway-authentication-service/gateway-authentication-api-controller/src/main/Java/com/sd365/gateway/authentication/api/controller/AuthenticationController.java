package com.sd365.gateway.authentication.api.controller;


import com.sd365.gateway.authentication.api.AuthenticationApi;
import com.sd365.gateway.authentication.service.AuthenticationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author yanduohuang
 * @version 1.0
 * @date 2020/12/17 20:57
 * @className
 * @description
 */
@Slf4j
@RestController
public class AuthenticationController implements AuthenticationApi {
    /**
     * 认证服务
     */
    @Autowired
    private  AuthenticationService authenticationService;

    @Override
    public String getToken(String code, String account, String password) {
       return authenticationService.getToken(code, account, password);
    }
}
