package com.sd365.gateway.authorization.service;

import com.sd365.gateway.authorization.entity.RoleResource;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * @Author 吴剑鸣
 * @Date 2020/12/18 12:51
 * @Version 1.0
 * @description
 */
public interface RoleResourcesService {

    List<Long> searchResourceIdsByroleIds (List<Long> roleIds);

}
