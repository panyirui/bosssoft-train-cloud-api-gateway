package com.sd365.gateway.authorization.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @class
 * @classdesc
 * @author Administrator
 * @date 2020-10-2  23:16
 * @version 1.0.0
 * @see
 * @since
 */
@Api(tags = "鉴权管理", value = "/v1")
@RestController
@RequestMapping(value = "/v1")
public interface Authorization {
    /**
     * @param:
     * @return:
     * @see
     * @since
     */
    @ApiOperation(tags = "鉴权判断url",value = "/authorization")
    @GetMapping(value = "/authorization")
    @ResponseBody
    Boolean roleAuthorization(String token,String url) throws InterruptedException;


    /**
     * @param:
     * @return:
     * @see
     * @since
     */
    @ApiOperation(tags = "开放URL查询",value = "/common/resource")
    @GetMapping(value = "/common/resource")
    @ResponseBody
    Boolean commonResource(String url) throws InterruptedException;

}
